import pandas as pd
from pandas_profiling import ProfileReport

df = pd.read_csv('../test_files/20220623-distritos.csv', delimiter=';')
# df.describe(include='all')

profile = ProfileReport(df)
profile.to_file(output_file='profile_distritos.html')
