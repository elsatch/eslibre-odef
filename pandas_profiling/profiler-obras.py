import pandas as pd
from pandas_profiling import ProfileReport

df = pd.read_excel('../test_files/20220623-obras.xlsx')
# df.describe(include='all')

profile = ProfileReport(df)
profile.to_file(output_file='profile_obras.html')
